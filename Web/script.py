import os
import json
import yaml

print("Inserisci il dominio di cui aggiungere la config: ")
domain = input()
print("Inserisci la Timezone: ")
timezone = input()

if timezone == "" :
    timezone = "Europe/Rome"

print('Dominio scelto: ' + domain)
print('Timeozne scelta: ' + timezone)

print("Sei sicuro di voler continuare? 1) Si 2) No")
choice = input()

def create_file(path, filename, content):
    if not os.path.exists(path):
        os.makedirs(path)
    filepath = os.path.join(path, filename)
    if os.path.exists(filepath):
        print('File already exists: ' + filepath)
    else:
        with open(filepath, 'w') as fp:
            fp.write(content)

if choice == "1":
    config = {}
    with open("docker-compose.yml", "r") as file:
        config = yaml.safe_load(file)
        parsed_domain = domain.replace(".", "_")
        if not config["services"]:
            config["services"] = {}
        config["services"][parsed_domain+"_nginx"] = {
            "container_name": "${COMPOSE_PROJECT_NAME}-" + parsed_domain + "-nginx",
            "build": {
                "context" : "./bin/" + parsed_domain + "/nginx",
                "args" : {
                    "TIMEZONE": timezone
                }
            },
            "restart": "unless-stopped",
            "ports" : [
                "80",
            ],
            "environment" : {
                "DOMAIN":domain,
                "DOMAIN_PARSED":parsed_domain
            },
            "volumes": [
                "./logs/" + parsed_domain + "/nginx:/var/log/nginx",
                "./data/" + parsed_domain + "/nginx:/www:ro",
                "./config/" + parsed_domain + "/nginx:/etc/nginx/templates:ro"
            ],
            "depends_on" : [
                parsed_domain + "_php"
            ],
            "networks" : [
                "web-internal",
                "npm-internal"
            ]
        }
        config["services"][parsed_domain+"_php"] = {
            "container_name": "${COMPOSE_PROJECT_NAME}-"+parsed_domain+"-php",
            "build":{
                "context": "./bin/"+parsed_domain+"/php",
                "args": {
                    "TIMEZONE":timezone
                },
            },
            "restart": "unless-stopped",
            "ports": [
                "9000",
            ],
            "volumes": [
                "./data/" + parsed_domain + "/nginx:/www:ro",
                "./config/" + parsed_domain + "/php/php.ini:/usr/local/etc/php/php.ini:ro"
            ],
            "networks": [
                "web-internal"
            ]
        }

        create_file("./bin/"+parsed_domain+"/nginx", "Dockerfile", "FROM nginx:alpine")
        create_file("./bin/"+parsed_domain+"/php","Dockerfile",
"""FROM php:fpm
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install mysqli""")
        create_file("./config/"+parsed_domain+"/nginx", "dafault.conf.template",
"""server {
    listen 80;
    index index.php index.html index.htm;
    server_name ${DOMAIN};
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /www;

    try_files $uri /index.html;

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${DOMAIN_PARSED}_php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param REQUEST_URI $request_uri;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    error_page 404 /index.html;
}""")
        create_file("./config/"+parsed_domain+"/php", "php.ini",
"""date.timezone = """ + timezone +
"""
display_errors = 1
error_reporting = E_ALL""")

    with open('docker-compose.yml', 'w') as file:
        yaml.dump(config, file, default_flow_style=False, sort_keys=False)
        print("Sito aggiunto al docker-compose!")
        print("Ho impostato le configurazioni predefinite, modificale come preferisci.")
        print("Ricordati di aggiungere il servizio ad NPM!")
else:
    print("Arrivederci!")
