Generate a valid matrix_key

Run the following command inside ./data/cagnocammello_com/dendrite_monolith

docker run --rm -it -v $(pwd):/key -w /key \
    --entrypoint /usr/bin/generate-keys matrixdotorg/dendrite-monolith:latest \
    --private-key matrix_key.pem

Register a new user:

docker exec -it dendrite-cagnocammello_com-dendrite_monolith /usr/bin/create-account -config /etc/dendrite/dendrite.yaml -username anulo2 -admin -url https://cagnocammello.com:8448

Sync certs
Run this command in the MatrixDendrite folder.
sudo rsync -avu --delete "../NPM_Portainer/data/letsencrypt/archive/npm-5/" "./data/cagnocammello_com/dendrite_monolith/certs" --chmod=755 --chown=anulo2:anulo2

If you use nginx proxy manager and your base domain (like cagnocammello.com for my instace) load something else than the dendrite instance
you should add this in the advanced tab, tweaked properly for your configuration

location ~ ^(/_matrix|/_synapse/client) {
    proxy_pass https://cagnocammello.com:8448;
    proxy_set_header X-Forwarded-For $remote_addr;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $host;

    client_max_body_size 500M;
}