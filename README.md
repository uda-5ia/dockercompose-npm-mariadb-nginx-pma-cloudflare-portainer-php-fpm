One docker-compose to rule them all,
one docker-compose to find them,
One docker-compose to bring them all
and in the darkness bind them.

# NPM config

Email: admin@example.com
Password: changeme


Note: if you set a network to be internal it can only access containers on the same network and NOT the external internet

Nextcloud tweaks:
when you add the onlyoffice and nextcloud domain add this in the advanced tab:
```
proxy_set_header Host $host;
proxy_set_header X-Forwarded-Proto $scheme;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_max_temp_file_size 16384m;
client_max_body_size 0;

location = /.well-known/carddav {
      return 301 $scheme://$host:$server_port/remote.php/dav;
    }
    location = /.well-known/caldav {
      return 301 $scheme://$host:$server_port/remote.php/dav;
    }
```
You only office config should be something similar to this:
![](./OnlyofficeConfig.png)
