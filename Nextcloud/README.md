
# Nextcloud

We need to add the cronjob to keep it updated.
`sudo cp ./cronjob/* /etc/systemd/system/`

`sudo systemctl enable nextcloudcron.timer`

`sudo systemctl start nextcloudcron.timer`

`sudo systemctl status nextcloudcron.timer`

Taken from: https://github.com/talesam/nextcloud/blob/main/README.md